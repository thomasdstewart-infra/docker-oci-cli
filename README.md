# docker-oci-cli

Gitea: [![Build Status](https://drone.stewarts.org.uk/api/badges/thomasdstewart-infra/docker-oci-cli/status.svg)](https://drone.stewarts.org.uk/thomasdstewart-infra/docker-oci-cli)

GitLab [![pipeline status](https://gitlab.com/thomasdstewart-infra/docker-oci-cli/badges/main/pipeline.svg)](https://gitlab.com/thomasdstewart-infra/docker-oci-cli/-/commits/main)

A Docker container to run the Oracle Cloud Infrastructure CLI

To use the ~/.oci/config file needs to be created using something like:

```
mkdir -p ~/.oci

cat <<EOF > ~/.oci/config
[DEFAULT]
user=$TF_VAR_user_ocid
fingerprint=$TF_VAR_fingerprint
key_file=~/.oci/oci_api_key.pem
tenancy=$TF_VAR_tenancy_ocid
region=$TF_VAR_region
EOF

cat ~/.oci/config

cat <<EOF > ~/.oci/oci_api_key.pem
$TF_VAR_private_key
EOF

chmod 600 ~/.oci/*
```
