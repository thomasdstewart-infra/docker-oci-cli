FROM docker.io/library/debian:bullseye
LABEL name="docker-oci-cli"
LABEL url="https://gitlab.com/thomasdstewart-infra/docker-oci-cli"
LABEL maintainer="thomas@stewarts.org.uk"

RUN    apt-get update \
    && apt-get -y --no-install-recommends install ca-certificates curl jq links \
    && curl -L -O https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh \
    && chmod +x ./install.sh \
    && ./install.sh --accept-all-defaults --install-dir /opt/oci-cli --exec-dir /usr/local/bin --script-dir /usr/local/bin \
    && apt-get clean \
    && rm -rf ./install.sh /var/lib/apt/lists/*

CMD ["oci"]
